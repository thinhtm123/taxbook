import React from "react";
import styles from "./FeaturesItem.module.css";
const FeaturesItem = props => {
  return (
    <h2 className={`${styles.features}`}>
      {props.heading1} <span style={{ display: "block" }}></span>{" "}
      <span style={{ color: "transparent", opacity: "0" }}></span>
      {props.heading2}
    </h2>
  );
};

export default FeaturesItem;
