import React from "react";
import styles from "./Intro.module.css";
import { Col, Container, Row } from "react-bootstrap";
import stepper from "../../img/stepper_circle.png";
import ios from "../../img/ios.png";
import android from "../../img/android.png";
import phone from "../../img/phone.png";

const Intro = () => {
  return (
    <section className={styles.intro}>
      <Container className={styles.container} fluid>
        <Row>
          <Col className="col-sm-12 col-md-6 col-lg-6">
            <div className={styles.wrapper}>
              <div className={styles.heading}>
                <span>TaxBook App </span>
                <p>
                  Công cụ không thể thiếu của chủ doanh nghiệp trong thời đại
                  4.0
                </p>
              </div>
              <div className={` ${styles.content} `}>
                <div className={styles.list}>
                  <div className={styles.text}>
                    <div>
                      <img className={styles.img} srcSet={`${stepper} 2x`} />
                    </div>
                    <p className={styles.lengthText}>
                      Ứng dụng dành riêng cho doanh nghiệp sử dụng dịch vụ của
                      đối tác từ nền tảng TaxBook. Đặc biệt,{" "}
                      <b>miễn phí trọn đời.</b>
                    </p>
                  </div>
                  <div className={styles.text}>
                    <div>
                      <img className={styles.img} srcSet={`${stepper} 2x`} />
                    </div>
                    <p className={styles.lengthText}>
                      Tra cứu mọi lúc, mọi nơi số liệu kế toán, nghĩa vụ thuế
                      của doanh nghiệp, từ báo cáo tổng hợp đến số liệu chi
                      tiết.
                    </p>
                  </div>
                  <div className={styles.text}>
                    <div>
                      <img className={styles.img} srcSet={`${stepper} 2x`} />
                    </div>
                    <p className={styles.lengthText}>
                      Nhận thông báo cập nhật chính sách về thuế, hóa đơn, lao
                      động, BHXH... để kịp thời có phương án xử lý tối ưu.
                    </p>
                  </div>
                </div>
              </div>
              <div className={styles.app}>
                <a href="">
                  <img src={ios} alt="" />
                </a>
                <a href="">
                  <img src={android} alt="" />
                </a>
              </div>
            </div>
          </Col>
          <Col className=" d-flex justify-content-end justify-content-sm-center col-12 col-sm-12 col-md-6 col-lg-6 `">
            <div className={styles.imgPhone}>
              <img srcSet={`${phone} 2x`} alt="" className={styles.phoneImg} />
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Intro;
