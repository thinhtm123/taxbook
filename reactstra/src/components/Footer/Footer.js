import React from "react";
import { Container, Row, Col } from "reactstrap";
import logo1 from "../../img/logo1.png";
import styles from "./Footer.module.css";
import Zalo from "../../img/Zalo.png";
import Linked from "../../img/linkedin.png";
import Twitter from "../../img/twitter.png";
import Facebook from "../../img/Facebook.png";
import Youtube from "../../img/youtube.png";

const Footer = () => (
  <footer className={styles.footer}>
    <Container fluid>
      <Row>
        <Col xs={12} lg={3}>
          <div className={styles.footer_logo}>
            <img src={logo1} />
          </div>
        </Col>
        <Col xs={12} lg={2} className={styles.about}>
          <h3 className={styles.footer_heading}>Về taxbook</h3>
          <ul className={styles.footer_links}>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Tầm nhìn & sứ mệnh
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Cơ hội nghề nghiệp
              </a>
            </li>
          </ul>
        </Col>
        <Col xs={12} lg={2} className={styles.product}>
          <h3 className={styles.footer_heading}>Sản phẩm</h3>
          <ul className={styles.footer_links}>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                TaxBook App
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                TaxBook Accounting
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Thỏa thuận sử dụng
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Chính sách quyền riêng tư
              </a>
            </li>
          </ul>
        </Col>
        <Col xs={12} lg={2} className={styles.support}>
          <h3 className={styles.footer_heading}>Hỗ trợ</h3>
          <ul className={styles.footer_links}>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Liên hệ
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                FAQ
              </a>
            </li>
            <li className="footer-item">
              <a href="#" className={styles.footer_link_text}>
                Thư viện
              </a>
            </li>
          </ul>
        </Col>
        <Col xs={12} lg={2} className={styles.social_position}>
          <h3 className={styles.footer_heading}>Social</h3>
          <div className={styles.social}>
            <a href="#" className={styles.social_item}>
              <img src={Zalo} alt="" />
            </a>
            <a href="#" className={styles.social_item}>
              <img src={Facebook} alt="" />
            </a>
            <a href="#" className={styles.social_item}>
              <img src={Linked} alt="" />
            </a>
            <a href="#" className={styles.social_item}>
              <img src={Youtube} alt="" />
            </a>
            <a href="#" className={styles.social_item}>
              <img src={Twitter} alt="" />
            </a>
          </div>
        </Col>
      </Row>
      <Row className={styles.border}></Row>
      <Row xs={12} lg={12}>
        <p className={styles.copyright}>
          Copyright © by 2022 Bản quyền thuộc Công Ty Cổ Phần Đầu Tư Công Nghệ
          Thiên Thần - TaxBook.vn
        </p>
      </Row>
    </Container>
  </footer>
);

export default Footer;
