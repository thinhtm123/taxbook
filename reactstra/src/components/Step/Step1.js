import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import styles from "./Step1.module.css";
import img1 from "../../img/step3.png";
import img2 from "../../img/step3sub.png";
const Step1 = () => {
  return (
    <section className={styles.step}>
      <Container fluid>
        <Row>
          <Col className=" col-12 col-sm-12 col-lg-6 col-md-4 ">
            <div className={`d-flex flex-column mb-3 ${styles.position}`}>
              <p className={styles.number}>1.</p>
              <p className={styles.text}>
                Cung cấp <br></br>
                <span className={`${styles.textpri} ${styles.textborder}`}>
                  thông tin <br></br>
                </span>
                cơ bản
              </p>
              <p className={styles.desc}>
                Trên cơ sở đó, TaxBook đề xuất <br></br>khoảng phí để chủ doanh
                nghiệp <br></br>tham khảo và đưa ra mức ngân <br></br>sách phù
                hợp.{" "}
              </p>
            </div>
          </Col>
          <Col className="col-12 col-sm-12 d-flex justify-content-end col-lg-6 col-md-8">
            <div className={styles.imgHead}>
              <img className={styles.imgParent} src={img1} />
              <img className={styles.imgChil} src={img2} />
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Step1;
