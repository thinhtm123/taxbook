import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import styles from "./Step3.module.css";
import img1 from "../../img/step3_3.png";
import img2 from "../../img/step3_3sub.png";
import { Button } from "react-bootstrap";
const Step3 = () => {
  return (
    <section className={styles.step}>
      <Container fluid>
        <Row>
          <Col className="d-flex flex-column align-items-start  col-12 col-lg-5 col-sm-6 col-md-4 ">
            <div
              className={`d-flex flex-column justify-content-end mb-3 ${styles.position}`}
            >
              <p className={styles.number}>3.</p>
              <p className={styles.text}>
                Lựa chọn <br></br>
                đơn vị <br></br>
                <span className={`${styles.textpri} ${styles.textborder}`}>
                  phù hợp
                </span>
              </p>
              <Col className={`${styles.desc} col-lg-6 col-sm-6`}>
                Sau khi nhận chào phí, chủ doanh nghiệp xem thông tin hồ sơ năng
                lực, đánh giá, xếp hạng... để cân nhắc lựa chọn đơn vị phù hợp
                nhất cho doanh nghiệp.
              </Col>
              <button
                type="button"
                className={` btn btn-primary ${styles.btn1}  `}
              >
                KẾT NỐI NGAY
              </button>
            </div>
          </Col>
          <Col className="col-12 col-sm-12 d-flex justify-content-end col-lg-7 col-md-8">
            <div className={styles.imgHead}>
              <img className={styles.imgParent} src={img1} />
              <img className={styles.imgChil} src={img2} />
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Step3;
