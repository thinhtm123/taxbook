import React from "react";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Step from "./Step";
const Index = () => {
  return (
    <div>
      <Step></Step>
      <Step1></Step1>
      <Step2></Step2>
      <Step3></Step3>
    </div>
  );
};

export default Index;
