import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import styles from "./Step2.module.css";
import img1 from "../../img/step3_2.png";
import img2 from "../../img/step3_2sub.png";
const Step2 = () => {
  return (
    <section className={styles.step}>
      <Container fluid>
        <Row>
          <Col className="col-12 col-lg-4 col-sm-6 col-md-4 ">
            <div className={`d-flex flex-column mb-3 ${styles.position}`}>
              <p className={styles.number}>2.</p>
              <p className={styles.text}>
                Nhận <br></br>
                <span className={`${styles.textpri} ${styles.textborder}`}>
                  chào phí<br></br>
                </span>
                dịch vụ
              </p>
              <Col className={`${styles.desc} col-lg-6 col-sm-12`}>
                TaxBook sẽ chuyển thông tin đến 05 đối tác dịch vụ phù hợp nhất
                trên nền tảng để chào phí dịch vụ. Chủ doanh nghiệp sẽ nhận được
                đề xuất phí cũng như thông tin về đối tác dịch vụ qua email,
                trong tối đa là 24 giờ.
              </Col>
            </div>
          </Col>

          <Col className="col-12 col-sm-12 d-flex justify-content-end col-lg-8 col-md-8">
            <div className={styles.imgHead}>
              <img className={styles.imgParent} src={img1} />
              <img className={styles.imgChil} src={img2} />
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Step2;
