import React from "react";
import { Row, Container, Col } from "react-bootstrap";
import styles from "./Step.module.css";
const Step = () => {
  return (
    <section className={styles.step}>
      <Container fluid>
        <Row className="col-lg-12 col-sm-12 col-md-12 ">
          <Col className="d-flex flex-column justify-content-center align-items-center">
            <span className={styles.textHead}>3 bước</span>
            <p className={`${styles.bordersimp}`}>đơn giản</p>
            <p className={`${styles.textsub}`}>
              để tìm kiếm đơn vị dịch vụ Kế toán - Thuế phù hợp
            </p>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Step;
