import React from "react";
import styles from "./Header.module.css";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";
const Header = () => {
  return (
    <header className={styles.header}>
      <Navbar
        className={`${styles.navPadding} ${styles.header}`}
        fixed="top"
        expand="lg"
        light
      >
        <NavbarBrand href="/">
          <img
            className={styles.navLogo}
            src="https://taxbook.vn/icons/logo.png"
            alt="Logo"
          />
        </NavbarBrand>
        <Collapse navbar className="text-center">
          <Nav
            className={`mx-auto gap-5 ${styles.navText} ${styles.border}`}
            navbar
          >
            <NavItem>
              <NavLink href="/">Trang chủ</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">Sản phẩm</NavLink>
            </NavItem>

            <NavItem>
              <NavLink href="/">Chuyên gia</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
        <Button size="lg" outline className={styles.ml_20}>
          Đăng nhập
        </Button>
        <NavbarToggler onClick={function noRefCheck() {}} />
      </Navbar>
    </header>
  );
};

export default Header;
