import React from "react";
import "./App.css";
import Header from "./components/Header/Header";
import Banner from "./pages/Home/Banner";
import Intro from "./components/Intro/Intro";
import Footer from "./components/Footer/Footer";
import CTA from "./pages/Home/CTA";
import Index from "./components/Step/Index";
function App() {
  return (
    <div>
      <Header></Header>
      <Banner></Banner>
      <CTA></CTA>
      <Index></Index>
      <Intro></Intro>
      <Footer></Footer>
    </div>
  );
}

export default App;
