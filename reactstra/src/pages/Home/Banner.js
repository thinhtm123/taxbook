import React, { useEffect, useRef, useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import FeaturesItem from "../../components/FeaturesItem";
import styles from "./Banner.module.css";
const Banner = () => {
  const videoRef = useRef();
  const buttonRef = useRef();
  const [isPlay, setIsPlay] = useState(false);
  const toggleVideo = () => {
    setIsPlay(pre => !pre);
  };
  useEffect(() => {
    if (isPlay) {
      videoRef.current.play();
      buttonRef.current.style.display = "none";
    } else {
      videoRef.current.pause();
      buttonRef.current.style.display = "block";
    }
  }, [isPlay]);
  return (
    <div className={styles.banner}>
      <Container className=" " fluid>
        <Row xs="1" md="1">
          <Col>
            <div className={`${styles.bannerContent}`}>
              <p style={{ marginBottom: "8px" }} className={styles.largeBold}>
                Bạn đang cần
              </p>
              <p
                style={{ marginBottom: "32px" }}
                className={` ${styles.largeBold} ${styles.semiBold}`}
              >
                Dịch vụ <span>Kế toán-Thuế?</span>
              </p>
              <p
                style={{ marginBottom: "48px" }}
                className={` ${styles.medMed}`}
              >
                TaxBook có hơn 500 tổ chức và chuyên gia đối tác trên nền tảng
              </p>
              <Button
                className="btn btn-primary btn-lg"
                style={{ margin: "auto" }}
              >
                KẾT NỐI NGAY
              </Button>
              <span className={`${styles.nextTo}`}>
                <svg
                  className={`${styles.icons}`}
                  xmlns="http://www.w3.org/2000/svg"
                  width="8"
                  height="21"
                  viewBox="0 0 8 21"
                  fill="none"
                >
                  {" "}
                  <path
                    d="M8 16.8H5L5 0L3 0L3 16.8H0L4 21L8 16.8Z"
                    fill="#263238"
                  />{" "}
                </svg>
              </span>
            </div>
          </Col>
        </Row>
        <Row
          style={{ padding: "150px 0 200px 0" }}
          className={`${styles.rowVideo}`}
        >
          <Col>
            <div className={`${styles.videoContainer}`}>
              <video
                onClick={toggleVideo}
                ref={videoRef}
                className={`${styles.video}`}
                src="https://www.sendpotion.com/assets/video/home/hero/1-full.mp4"
              ></video>
              <span
                onClick={toggleVideo}
                ref={buttonRef}
                className={`${styles.videoIcon}`}
              >
                <svg
                  className={`${styles.centerItem}`}
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="40"
                  viewBox="0 0 32 40"
                  fill="none"
                >
                  <path
                    d="M0.445312 39.9722L31.3064 20.3333L0.445312 0.694427V39.9722Z"
                    fill="#2E3A59"
                  />
                </svg>
              </span>
            </div>
          </Col>
        </Row>
        <Row
          style={{ padding: "150px 0 200px 0" }}
          className={`${styles.rowVideo}`}
        >
          <Col className="col-lg-12">
            {/* <h2 className={`${styles.features}`}>
                            Nhanh chóng{" "}
                            <span style={{ display: "block" }}></span>
                            <span
                                style={{ color: "transparent", opacity: "0" }}
                            >
                                {" "}
                                .
                            </span>{" "}
                            & hiệu suất
                        </h2> */}{" "}
            <FeaturesItem
              heading1=" Nhanh chóng"
              heading2=" & hiệu suất"
            ></FeaturesItem>
          </Col>
        </Row>{" "}
        <Row
          style={{ padding: "150px 0 200px 0" }}
          className={`${styles.rowVideo}`}
        >
          <Col className="col-lg-12">
            {/* <h2 className={`${styles.features}`}>
                            Am hiểu <span style={{ display: "block" }}></span>{" "}
                            <span
                                style={{ color: "transparent", opacity: "0" }}
                            >
                                {" "}
                                .
                            </span>
                            chuyên môn
                        </h2> */}
            <FeaturesItem
              heading1="Am hiểu"
              heading2="chuyên môn"
            ></FeaturesItem>
          </Col>
        </Row>
        <Row
          style={{ padding: "150px 0 200px 0" }}
          className={`${styles.rowVideo}`}
        >
          <Col className="col-lg-12">
            {/* <h2 className={`${styles.features}`}>
                            <span
                                style={{ color: "transparent", opacity: "0" }}
                            >
                                {" "}
                                .
                            </span>
                            Bảo mật <span
                                style={{ display: "block" }}
                            ></span>{" "}
                            <span
                                style={{ color: "transparent", opacity: "0" }}
                            >
                                {" "}
                                .
                            </span>{" "}
                            & an toàn
                        </h2> */}{" "}
            <FeaturesItem
              heading1="Bảo mật "
              heading2="& an toàn"
            ></FeaturesItem>
          </Col>
        </Row>{" "}
      </Container>
    </div>
  );
};

export default Banner;
