import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import styles from "./CTA.module.css";
const CTA = () => {
  return (
    <Container className={`${styles.section}`} fluid>
      <Row>
        <Col>
          <div className={`${styles.textContent}`}>
            <p style={{ marginBottom: "8px" }} className={`${styles.title}`}>
              Hãy
              <span> tập trung tối ưu </span>kinh doanh!
            </p>
            <p style={{ marginBottom: "48px" }} className={` ${styles.desc}`}>
              Để TaxBook và các Đối tác giúp bạn “quẳng nỗi lo” về Thuế & Kế
              toán.
            </p>
            <Button className="btn btn-primary btn-lg">KẾT NỐI NGAY</Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default CTA;
